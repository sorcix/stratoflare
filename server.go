package main

import (
	"io"
	"log"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
)

var (
	client                       = http.DefaultClient
	accounts map[string]*Account = make(map[string]*Account)
)

type Config struct {
	CloudflareKey string
	Accounts      []struct {
		CloudflareKey string
		Key           string
		Endpoints     []struct {
			Regex      string
			Attributes map[string]string
		} `toml:"Endpoint"`
	} `toml:"Account"`
}

type Endpoint struct {
	Regex      *regexp.Regexp
	Attributes map[string]*regexp.Regexp
}

type Account struct {
	CloudflareKey string
	Endpoints     []*Endpoint
}

func readConfig() {
	var (
		config = &Config{}
		err    error
	)

	if _, err = toml.DecodeFile("stratoflare.cfg", config); err != nil {
		log.Fatal(err.Error())
	}

	// Convert Config struct to internal representation
	for _, account := range config.Accounts {

		a := &Account{}
		if len(account.CloudflareKey) < 1 {
			a.CloudflareKey = config.CloudflareKey
		} else if len(config.CloudflareKey) < 1 {
			a.CloudflareKey = account.CloudflareKey
		} else {
			log.Fatalf("No cloudflare API key for account: %s", account.Key)
		}

		a.Endpoints = make([]*Endpoint, len(account.Endpoints), len(account.Endpoints))
		for i, endpoint := range account.Endpoints {
			e := &Endpoint{
				Attributes: make(map[string]*regexp.Regexp),
			}
			if e.Regex, err = regexp.Compile(endpoint.Regex); err != nil {
				log.Fatalf("Unable to compile regex: %s", endpoint.Regex)
			}
			for k, v := range endpoint.Attributes {
				if e.Attributes[k], err = regexp.Compile(endpoint.Regex); err != nil {
					log.Fatalf("Unable to compile regex: %s", v)
				}
			}
			a.Endpoints[i] = e
		}

		accounts[account.Key] = a
	}
}

func main() {

	readConfig()

	mux := http.NewServeMux()
	mux.HandleFunc("/client/v4/", apiHandler)
	mux.HandleFunc("/", errorHandler)

	server := &http.Server{
		Addr:           "0.0.0.0:8080",
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Printf("Server starting: %s", server.Addr)

	log.Fatal(server.ListenAndServe())
}

func errorHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("Request (useless): %s from %s\n", r.URL.Path, r.RemoteAddr)
	http.Error(w, "I have no idea what you want, sorry..", http.StatusBadRequest)
}

func apiHandler(w http.ResponseWriter, r *http.Request) {

	var (
		account *Account
		ok      bool
	)

	log.Printf("Request: %s from %s\n", r.URL.Path, r.RemoteAddr)

	if account, ok = accounts[r.Header.Get("X-Auth-Key")]; !ok {
		http.Error(w, "Stratoflare proxy key is not allowed.", http.StatusUnauthorized)
		return
	}

	// Modify request to send to Cloudflare
	r.URL.Host = "api.cloudflare.com"
	r.URL.Scheme = "https"
	r.Host = ""
	r.RequestURI = ""

	allowed := false
	for _, endpoint := range account.Endpoints {
		if !endpoint.Regex.MatchString(strings.TrimPrefix(r.URL.Path, "/client/v4")) {
			continue
		}

		// We have an endpoint match!
		allowed = true
	}

	if !allowed {
		http.Error(w, "Stratoflare proxy key is not allowed to execute this request!", http.StatusForbidden)
		return
	}

	// Overwrite X-Auth-Key header
	r.Header.Set("X-Auth-Key", account.CloudflareKey)

	var (
		resp *http.Response
		err  error
	)

	if resp, err = client.Do(r); err != nil {
		log.Printf("Error: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(resp.StatusCode)
	if _, err = io.Copy(w, resp.Body); err != nil {
		log.Printf("Error: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	return
}
