GOBUILD := CGO_ENABLED=0 GOPATH=$(GOPATH) go build -ldflags '-w -s' -a -o

prepare:
	go get -u github.com/BurntSushi/toml

build: prepare
	$(GOBUILD) stratoflare

.PHONY: prepare build
